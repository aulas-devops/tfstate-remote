terraform {
  required_version = "1.1.4"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.15.1"
    }

    random = {
      source  = "hashicorp/random"
      version = "3.2.0"
    }
  }

  backend "s3" {}

}

provider "aws" {
  region  = var.aws_region
  profile = "tf114"
}

resource "aws_instance" "api" {
  ami           = var.instance_ami
  instance_type = var.instance_type

  tags = var.instance_tags
}

